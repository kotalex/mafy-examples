import React, { useContext } from 'react';
import { makeStyles, Typography } from '@material-ui/core';
import { useParams } from 'react-router-dom';
import { OnboardingEventsContext } from '../contexts/OnboardingEventsContext';
import UserContext from '../contexts/UserContext';
import CreateEventModal from '../onboarding/create/CreateEventModal';

const useStyles = makeStyles((theme) => ({
    container: {
        padding: theme.spacing(2),
    },
    marginBottom: {
        marginBottom: theme.spacing(2),
    },
}));

const OnboardingEventRenderer = () => {
    const classes = useStyles();
    const { eventId } = useParams();
    const { language } = useContext(UserContext);
    const onboardingEvents = useContext(OnboardingEventsContext);
    const onboardingEvent = onboardingEvents?.filter(
        (event) => event.id === eventId,
    )[0];

    return (
        <div className={classes.container}>
            <Typography className={classes.marginBottom} variant="h5">
                {onboardingEvent?.steps.map((s) => s[language].title).join(' >\xa0')}
            </Typography>
            <CreateEventModal data={onboardingEvent} />
        </div>
    );
};

export default OnboardingEventRenderer;
