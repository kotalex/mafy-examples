import React from 'react';
import Joyride, {Step} from 'react-joyride';
import { useTranslation } from 'react-i18next';

const { t } = useTranslation();

function OnboardingStepper(steps: Step[]) {
  return (
    <Joyride
      steps={steps}
      continuous
      showProgress
      showSkipButton
      hideCloseButton
      disableOverlayClose
      // If no dimming wanted: disableOverlay
      locale={{
        skip: t('Skip'),
        next: t('Next'),
        last: t('Close'),
        back: t('Back'),
      }}
      styles={{
        buttonNext: {
          background: '#003B7E',
          fontFamily: 'Poppins, Open Sans',
        },
        buttonBack: {
          color: '#000000',
          fontSize: 16,
          fontFamily: 'Poppins, Open Sans',
        },
        buttonSkip: {
          color: '#000000',
          fontSize: 16,
          fontFamily: 'Poppins, Open Sans',
        },
        buttonClose: {
          color: '#000000',
          fontSize: 16,
          fontFamily: 'Poppins, Open Sans',
        },
        tooltip: {
          color: '#000000',
          fontSize: 16,
        },
        tooltipTitle: {
          fontSize: 16,
        },
      }}
    />
  );
};

export default React.memo(OnboardingStepper);