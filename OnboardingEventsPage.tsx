import React, { useContext, useMemo } from 'react';
import {
  makeStyles,
  Typography,
  Grid,
  CircularProgress,
} from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import UserContext from '../contexts/UserContext';
import CreateEventModal from '../onboarding/create/CreateEventModal';
import { OnboardingEventsContext } from '../contexts/OnboardingEventsContext';
import OnboardingEventDocumentCard from './OnboardingEventDocumentCard';

const useStyles = makeStyles((theme) => ({
  container: {
    padding: theme.spacing(2),
  },
  marginBottom: {
    marginBottom: theme.spacing(2),
  },
}));

const OnboardingEventPage = () => {
  const classes = useStyles();
  const { t } = useTranslation();
  const { language } = useContext(UserContext);
  const onboardingEvents = useContext(OnboardingEventsContext);

  const sortedOnboardingEvents = useMemo(
    () => {
      if (onboardingEvents === undefined || onboardingEvents === null) {
        return onboardingEvents
      }

      return onboardingEvents
        .slice()
        .sort((a, b) =>
          a.steps[0][language].title.localeCompare(b.steps[0][language].title)
        )
    },
    [onboardingEvents]
  );

  const renderOnboardingEvents = (events: OnboardingEvent[] | null | undefined) => {
    if (events === undefined) {
      return <CircularProgress />
    }
    else if (events === null) {
      return (
        <Typography>
          {t('Fetching existing onboarding events failed')}
        </Typography>
      )
    }
    else if (events.length === 0) {
      return (
        <Typography>
          {t('There are no existing onboarding events')}
        </Typography>
      )
    }

    return (
      <>
        <Typography className={classes.marginBottom} variant="h5">
          {t('Onboarding events')}
        </Typography>
        <Grid container spacing={2}>
          {events.map((onboardingEvent) => (
            <Grid
              item
              xs={12}
              sm={6}
              md={4}
              key={`onboarding-event-${onboardingEvent.id}`}
            >
              <OnboardingEventDocumentCard data={onboardingEvent} />
            </Grid>
          ))}
        </Grid>
      </>
    )
  };

  return (
    <div className={classes.container}>
      <div className={classes.marginBottom}>
        <CreateEventModal />
      </div>
      {renderOnboardingEvents(sortedOnboardingEvents)}
    </div>
  );
};

export default OnboardingEventPage;
